import { applyMiddleware, createStore } from 'redux';
import combineReducers from './reducers/index';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';


const composeEnhancers = composeWithDevTools({
    // Specify name here, actionsBlacklist, actionsCreators and other options if needed
});

const middleware = [
    thunk
];

let store = createStore((combineReducers), 

    composeEnhancers(
        applyMiddleware(...middleware),
    )
);

export default store;