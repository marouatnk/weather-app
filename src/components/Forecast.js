
import React from 'react';
import { connect } from 'react-redux';
import { fetchForecast } from '../actions/forecast';
import ForecastResult from "./ForecastResult";
import ForecastTitle from "./ForecastTitle";
import { bindActionCreators }  from 'redux';
import ForecastForm from './ForecastForm';


function Forecast() {
  return (
    <div>
        <ForecastTitle />
        <ForecastResult />
        <ForecastForm />
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
      forecast: state.forecastReducer.forecast,
  }
}


function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchForecast }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(Forecast)