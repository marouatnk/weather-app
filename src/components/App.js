import React from 'react';
import Forecast from './Forecast';

function App() {
  return (
    <div className="App">
        <Forecast />
    </div>
  );
}

export default App
