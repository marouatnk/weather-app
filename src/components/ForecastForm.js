import React from 'react';
import { bindActionCreators } from 'redux';
import {connect} from "react-redux";
import { fetchForecast } from '../actions/forecast';


class ForecastForm extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            'city' : 'Lyon',
            // 'temperature' : '30'
        }
    }

    changeCity = (event) => {
        this.setState({
            'city' : event.target.value,
            // 'temperature' : event.target.value
        })
    }

    fetchForecast = () => {
        this.props.fetchForecast(this.state.city);
    }

    render(){
        return (
            <div>
                <input onChange={this.changeCity} value={this.state.city} type="text" />
                <button onClick={this.fetchForecast}>Rechercher une ville</button>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {

    }
  }

  function mapDispatchToProps(dispatch) {
    return bindActionCreators({
      fetchForecast, 
    }, dispatch)
  }

  export default connect(mapStateToProps, mapDispatchToProps)(ForecastForm)