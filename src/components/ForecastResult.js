import React from 'react';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";

class ForecastResult extends React.Component{

    render(){
        return (
            <div>
                <p>{this.props.forecast.location.name}</p>
                <p>{this.props.forecast.current.temperature}°c</p>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        forecast: state.forecastReducer.forecast,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({

    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ForecastResult)
